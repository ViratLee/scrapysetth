import scrapy, re, json, io, configparser
from string import ascii_uppercase
from bs4 import BeautifulSoup
from time import sleep
from scrapysetth.items import SymbolDetail, AllSymbol, Symbol
class SymbolSpider(scrapy.Spider):
    name='SymbolSpider'
    output_json = None
    main_config = None
    all_symbol_count = 0
    data = {}
    data['all'] = []

    def read_config(self):
        config = configparser.ConfigParser()
        config.read('set_config.ini')
        self.main_config = config['main']

    def generate_list(self):
        url_domain = self.main_config['url.domain']
        urls=['{}?language=th&country=TH&prefix={}'.format(url_domain,'NUMBER')]
        for i in ascii_uppercase:
            urls.append('{}?language=th&country=TH&prefix={}'.format(url_domain,i))
        return urls

    def start_requests(self):
        self.read_config()
        self.output_json = self.main_config['file.name']
        urls = self.generate_list()
        return [scrapy.Request(url=url,encoding='utf-8', callback=self.parse)
                for url in urls]
    def parse(self, response):
        url = response.url
        print(type(response))
        soup = BeautifulSoup(response.text, 'html.parser')
        count = 0
        
        for table_data in soup.findAll('table', attrs={'class': re.compile("(?=.*table)(?=.*table-profile)")}):
            print(type(table_data))
            print('------------')
            #print(table_data)
            #symbolDetails = []
            all_tr = table_data.find_all('tr', class_= None)
            for tr in all_tr:
                td_tag = tr.find_all('td')
                #print(type(td_tag))
                print(td_tag[0].text.strip())
                count += 1
                symbol = Symbol(td_tag[0].text.strip(),td_tag[0].find('a').attrs['href'], td_tag[1].text.strip())
                self.data['all'].append(symbol.__dict__)
                #self.data['all'] = np.append(self.data['all'],[symbol.__dict__])
        print('----TOTAL:{}'.format(count))
        self.all_symbol_count = self.all_symbol_count + count
        self.log('waiting 5 second....')
        sleep(5)
      
#        return al
    def closed(self, reason):
        print('text close {}'.format(self.all_symbol_count))
        self.saveJsonfile()
        

    def saveJsonfile(self):
        self.log('Saved file {}, total count {}'.format(self.output_json,self.all_symbol_count))
        with open(self.output_json, 'a+', encoding='utf8') as f:
            parsed = json.dumps(self.data, ensure_ascii=False, indent=4) 
            f.write(parsed)
