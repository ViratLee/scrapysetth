# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


#class ScrapysetthItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
#    pass
class AllSymbol(scrapy.Item):
	symbolDetails = scrapy.Field()

class SymbolDetail(scrapy.Item):
	symbol = scrapy.Field()
	link = scrapy.Field()

class Symbol(object):
    def __init__(self, symbol, link, description):
        self.symbol = symbol
        self.link = link
        self.description = description